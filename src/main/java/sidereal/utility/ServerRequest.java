package sidereal.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.Arrays;


public class ServerRequest {
	public static byte[] POST(String URL, byte[] data) throws IOException {
		return ServerRequest.request(URL, "POST", data);
	}

	public static byte[] PUT(String URL, byte[] data) throws IOException {
		return ServerRequest.request(URL, "PUT", null);
	}

	public static byte[] GET(String URL) throws IOException {
		return ServerRequest.request(URL, "GET", null);
	}

	public static byte[] DELETE(String URL) throws IOException {
		return ServerRequest.request(URL, "DELETE", null);
	}

	private static byte[] request(String URL, String method, byte[] data)
			throws IOException {
		
		if(!URL.startsWith("http://")) URL = "http://"+URL;
		URL obj = new URL(URL);

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod(method);
		con.setDoInput(true);

		if (data != null) {
			con.setRequestProperty("Content-Length",
					Integer.toString(data.length));
			con.setRequestProperty("Content-Language", "en-US");
			con.setRequestProperty("Content-type", "text/html; charset=utf-8");

			con.setUseCaches(false);
			con.setDoOutput(true);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.write(data);
			wr.flush();
			wr.close();
		}

		int responseCode = con.getResponseCode();

		System.out.println("Made a " + method + " request to "+URL+", response code is "
				+ responseCode);
		return getDataFromStream(con.getInputStream());

	}

	public static byte[] getDataFromStream(InputStream stream) {
		try {
			byte[] data = new byte[16];

			int currIndex = 0;
			byte currentbyte = (byte) stream.read();
			while (currentbyte != -1) {
				data[currIndex] = currentbyte;
				currentbyte = (byte) stream.read();
				currIndex++;

				// expand array if necessary to 150% of its prev size
				if (data.length <= currIndex)
					data = Arrays.copyOf(data, (int) (data.length * 2f));
			}

			data = Arrays.copyOf(data, currIndex);

			return data;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[0];
	}
	
	public static void sendToTCPServer(String url, byte[] data)
	{
		try {
			
	         Socket socket = new Socket(url.split(":")[0], Integer.parseInt(url.split(":")[1]));
	         DataOutputStream toServer = new DataOutputStream(socket.getOutputStream()); 
	         toServer.write(data);
	         toServer.close();
	         socket.close();
	         WeakReference<Socket> gc = new WeakReference<Socket>(socket);

	      }
	      catch(Exception e) {
	         System.out.print("Whoops! It didn't work! Reason: "+e.getMessage() + " ");
	      }
	}

	public static byte[] getDataFromStream(BufferedReader reader) {
		try {
			byte[] data = new byte[16];

			int currIndex = 0;
			byte currentbyte;

			System.out.println(reader.readLine());

			while ((currentbyte = (byte) reader.read()) != -1) {
				data[currIndex] = currentbyte;
				currIndex++;

				// expand array if necessary to 150% of its prev size
				if (data.length <= currIndex)
					data = Arrays.copyOf(data, (int) (data.length * 1.5f));
			}

			data = Arrays.copyOf(data, currIndex + 1);

			return data;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[0];
	}

}
