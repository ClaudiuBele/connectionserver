package sidereal.utility;

import java.security.Key;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {

	private static String key = "Bar12345Bar12345"; // 128 bit key
	
	public static byte[] encrypt(byte[] data) {

		if(data.length ==0) return data;
		
		try {
			// Create key and cipher
			SecretKeySpec  aesKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			// encrypt the text
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);


			byte[] bytes;
			if (data.length % 16 != 0) {
				bytes = new byte[(data.length / 16 + 1) * 16];
				for (int i = 0; i < bytes.length; i++) {
					if (i < data.length)
						bytes[i] = data[i];
					else
						bytes[i] = 0;
				}
			} else {
				bytes = data;
			}

			byte[] newbytes = cipher.doFinal(bytes);

			return newbytes;

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		return null;

	}

	public static byte[]  decrypt(byte[] data) {

		if(data.length ==0) return data;
		
		data = Arrays.copyOf(data, data.length/16 * 16);
		
		try {
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			byte[] bytesToProcess = cipher.doFinal(data);

			int currIndex = bytesToProcess.length - 1;
			int padding = 0;
			while (bytesToProcess[currIndex] == 0) {
				padding++;
				currIndex--;
			}
			byte[] bytes = (Arrays.copyOf(bytesToProcess,
					bytesToProcess.length - padding));
			
			return  bytes;

			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;

	}
}
