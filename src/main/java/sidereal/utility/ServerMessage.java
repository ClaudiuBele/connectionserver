package sidereal.utility;

public class ServerMessage{
	
		public static final int SERVER_CONNECTION_ADDED = 0;
		public static final int SERVER_CHAT_ADDED = 1;

		public static final int SERVER_DUPLICATE_FOUND = 2;
		public static final int SERVER_API_MISMATCH_CONNECTION = 3;
		public static final int SERVER_API_MISMATCH_CHAT = 4;
		
		public static final int DATA_UNREADABLE = 5;

		public static final int CHATROOM_FULL = 6;
		public static final int CHATROOM_ENTERED = 7;
		public static final int CHATROOM_CREATED = 8;
		public static final int CHATROOM_LEFT = 9;
		public static final int CHATROOM_MESSAGE_SENT = 10;
		public static final int CHATROOM_ALREADY_JOINED = 11;
		
		public static final int CLIENT_ENTERED = 12;
		public static final int CLIENT_NOT_AUTHENTICATED = 13;
		public static final int CLIENT_TIMEOUT = 14;

		
		public static String toString(int value)
		{
			switch(value)
			{
				case SERVER_CHAT_ADDED: 				return "Chat server has been successfully added to the list of servers";
				case SERVER_CONNECTION_ADDED: 			return "Connection Server has been successfully added to the list of servers";
				case SERVER_DUPLICATE_FOUND: 			return "Server has not been added because it has already been registered";
				case SERVER_API_MISMATCH_CONNECTION: 	return "Connection server does not fully implement the intended API implemented";
				case SERVER_API_MISMATCH_CHAT:			return "Chat server does not fully implement the inteded API implemented";
				
				case CHATROOM_FULL:						return "Chat room is full";
				case CHATROOM_ENTERED:					return "Chat room has been entered";
				case CHATROOM_CREATED:					return "Chat room has been created";
				case CHATROOM_LEFT:						return "Client has left the chatroom";
				case CHATROOM_MESSAGE_SENT:				return "Message has been successfully sent";
				case CHATROOM_ALREADY_JOINED:			return "Already joined chat room";
				
				case CLIENT_ENTERED:					return "Client has succesfully connected to the server";
				case CLIENT_NOT_AUTHENTICATED: 			return "Client has not logged in the system";
				case CLIENT_TIMEOUT:					return "Client has timed out due to inactivity";
				
				case DATA_UNREADABLE:					return "Data to extract from stream is unreadable";
				default: return " Default server exception. Something has occured";
			}
		}
		
	}