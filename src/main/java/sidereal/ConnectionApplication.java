package sidereal;

import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.context.annotation.ComponentScan;

import sidereal.api.ConnectionController;
import sidereal.objects.ChatServer;
import sidereal.utility.Encryption;
import sidereal.utility.ServerRequest;

@ComponentScan
@EnableAutoConfiguration
public class ConnectionApplication {

	// set the mapping of the connection servers to register to.
	public static final List<String> informationServers = new ArrayList<String>();

	private static ConnectionApplication instance = new ConnectionApplication();

	private final List<ChatServer> servers = new ArrayList<ChatServer>();
	
	private long updateChatServersTimestamp = 0;

	/**
	 * Server creator. The server expects that argument can be provided, stating Information servers to contact. The format of the application is the following:
	 * <p>
	 * <code>"java -jar <jarname> <servers=*server1&server2&server3&...> ..."</code>
	 * 
	 * <p>
	 * the <b>servers value</b> has to contain one or more servers, separated using the "&" character. The servers have
	 * to also contain the port. If no value is given, a default one will be given for the localhost at port 8080
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// start server
		SpringApplication.run(ConnectionApplication.class, args);

		informationServers.add("localhost:50354");

		
		// parse arguments into server name, flag and server address.
		for (int i = 0; i < args.length; i++) {
			
			// handle information servers
			if (args[i].startsWith("servers="))
				for (int j = 0; j < args[i].split("=")[1].split("&").length; j++) {
					informationServers.add(args[i].split("=")[1].split("&")[j]);
				}

		}
		

		// set name for connection server. As opposed to the chat server, a
		// connection server does not need a
		// readable name.
		WeakReference<Random> rand = new WeakReference<Random>(new Random());
		final String serverName = "Connection_Server_" +System.currentTimeMillis() + "_" + rand.get().nextInt(100000);

		// make a timer task that when ran, will contact all information servers with its own information
		// this is done in order to make sure we don't need to detect if an IS gets down
		TimerTask ISTask = new TimerTask() {

			public void run() {

				// for each connection server, we make
				for (int i = 0; i < informationServers.size(); i++) {
					// make final variable to use in annonymous inner class
					final String infoServer = informationServers.get(i);

					try {

						byte[] responseData = ServerRequest.POST("http://" + infoServer + "/connectionServer",
								Encryption.encrypt(serverName.getBytes()));

						// no chat servers, skip this information server
						if (responseData.length == 0)
							continue;

						System.out.println(responseData.length);
						responseData = Encryption.decrypt(responseData);
						String response = new String(responseData, "UTF-8");

						// register the chat servers passed by the information server.
						// if no servers are returned, no chat servers are currently connected to the information
						// server
						if (response.length() != 0)
							get().registerChatServers(response);

					} catch (Exception e) {

						e.printStackTrace();
						System.out.println("Unable to contact information server. Reson: " + e.getMessage());
					}

				}

			};
		};
		Timer ISTimer = new Timer();
		ISTimer.scheduleAtFixedRate(ISTask, 0, 60000);

		TimerTask ChSTask = new TimerTask() {

			@Override
			public void run() {

				for (int i = 0; i < instance.servers.size(); i++) {

						instance.updateChatServersTimestamp = System.currentTimeMillis();
						
						
						byte[] data = new byte[10];
						
						try{
							data = ServerRequest.GET(instance.servers.get(i).getFullAddress() + "/usage");

						}
						catch(Exception e)
						{
							// error occured, removing chat server.
							// if "connection refused: connect", server is down
							System.out.println("Removing chat server after trying to get load. Reason: " + e.getMessage());
							instance.servers.remove(i);
							i--;
							continue;
						}
						
							String text;
							try {
								text = new String(Encryption.decrypt(data), "UTF-8");
								instance.servers.get(i).dbCallsPerMinute = Long.parseLong(text.split("\\{V\\}")[0]);
								instance.servers.get(i).requestsPerMinute = Long.parseLong(text.split("\\{V\\}")[1]);

								System.out.println(instance.servers.get(i).requestsPerMinute + " "
										+ instance.servers.get(i).dbCallsPerMinute);
							} catch (UnsupportedEncodingException e) {
							}

							
						
						
				}	
			}
		};
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(ChSTask, 60000, 60000);

	}

	/**
	 * Registers chat servers found by splitting the {@link Spring} parameter. The method is called from
	 * {@link ConnectionApplication#main(String[])}, when signing up to the Information Server.
	 * <p>
	 * When a chat server connects to the same information server, all of the currently-connected connection servers are
	 * contacted by calling a POST in <code>connectionServerAddress/chatServers</code>, which is bound to
	 * {@link ConnectionController#addChatServers(byte[])}.
	 * 
	 * @param data
	 *            Decrypted data.
	 */
	public void registerChatServers(String data) {
		System.out.println(data + " ");

		String[] chatServers = data.split("\\{O\\}");

		// go over individual chat servers
		for (int i = 0; i < chatServers.length; i++) {

			// process individual chat server data into individual chat server fields
			String[] chatServerData = chatServers[i].split("\\{V\\}");
			String serverName = chatServerData[0];
			String serverAddress = chatServerData[1];
			String serverFlag = chatServerData[2];

			// go over currently-known chat servers, if we already know the
			// chat server that is passed, ignore.
			boolean found = false;
			for (int j = 0; j < servers.size(); j++) {
				if ( serverAddress.equals(servers.get(i).getFullAddress())) {
					
					// update data to an existing server
					servers.get(i).setServerFlag(serverFlag);
					servers.get(i).setServerName(serverName);
					found = true;
					break;
				}
			}

			// didn't find current server passed by information server in our list of
			// servers, connect to it.
			if (!found) {

				System.out.println("Adding chat server with address " + serverAddress + " and name " + serverName);
				servers.add(new ChatServer(serverName, serverAddress, serverFlag));
			}

		}
	}

	public static ConnectionApplication get() {
		return instance;
	}

	public List<ChatServer> getServers() {
		return servers;
	}

	public long getUpdateChatServersTimestamp() {
		return updateChatServersTimestamp;
	}
}
