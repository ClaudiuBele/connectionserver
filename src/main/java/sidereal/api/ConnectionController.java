package sidereal.api;

import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sidereal.ConnectionApplication;
import sidereal.objects.ChatServer;
import sidereal.utility.Encryption;

@RestController
public class ConnectionController {

	@RequestMapping(value = "/testConnection", method = RequestMethod.GET)
	public final byte[] testConnection() {
		return Encryption.encrypt(new String("true").getBytes());

	}

	@RequestMapping(value = "/chatServers", method = RequestMethod.GET)
	public final byte[] getChatServers() {
		
		System.out.println("Request for chat servers came");
		
		// build a list of chat servers to send to the user
		StringBuilder builder = new StringBuilder();
		List<ChatServer> chatServers = ConnectionApplication.get()
				.getServers();
		
		builder.append(System.currentTimeMillis() - ConnectionApplication.get().getUpdateChatServersTimestamp());
		builder.append("{M}");
		

		for (int i = 0; i < chatServers.size(); i++) {
			builder.append(chatServers.get(i).toString());
			if (i != chatServers.size() - 1)
				builder.append("{O}");
		}

		System.out.println("data to write back "+builder.toString());
		
		byte[] bytes = builder.toString().getBytes();
		new SoftReference<StringBuilder>(builder);
		return Encryption.encrypt(bytes);
	}

	@RequestMapping(value = "/chatServers", method = RequestMethod.POST)
	public final byte[] addChatServers(@RequestBody byte[] data) {

		data = Encryption.decrypt(data);

		if (data.length == 0)
			return Encryption.encrypt("false".getBytes());
		try {

			ConnectionApplication.get().registerChatServers(
					new String(data, "UTF-8"));
			
			return Encryption.encrypt("true".getBytes());

		} catch (UnsupportedEncodingException e) {

			return Encryption.encrypt("false".getBytes());
		}
	}
}
