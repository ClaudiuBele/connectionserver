package sidereal.objects;

public class ChatServer {

	private String serverName;
	private String fullAddress;
	private String serverFlag;
	public long requestsPerMinute;
	public long dbCallsPerMinute;
	
	public ChatServer(String serverName,String fullAddress,  String flag)
	{
		this.serverName = serverName;
		this.fullAddress = fullAddress;
		this.serverFlag = flag;
		this.requestsPerMinute = 0;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	
	public void updateServerStatus(long requestsPerMinute, long dbCallsPerMinute)
	{
		this.requestsPerMinute = requestsPerMinute;
		this.dbCallsPerMinute = dbCallsPerMinute;
	}
	
	public String toString()
	{
		return serverName+"{V}"+fullAddress+"{V}"+serverFlag+"{V}"+requestsPerMinute+"{V}"+dbCallsPerMinute;
	}

	public String getServerFlag() {
		return serverFlag;
	}

	public void setServerFlag(String serverFlag) {
		this.serverFlag = serverFlag;
	}
	
}
